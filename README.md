---
component: Home
---

Preview = [https://polar-falls-15159.herokuapp.com/ ](https://polar-falls-15159.herokuapp.com/ )

# ERC20 Token Generator - DAPP

Easily deploy Smart Contract for a Standard, Capped, Mintable, Burnable, Payable ERC20 Token.


Code created using [VuePress](https://vuepress.vuejs.org/).

## Smart Contracts Source
 
Discover Smart Contracts source [here](https://gitlab.com/eugenekyselev/erc20-generator).

## Install dependencies

```bash
npm install
```

## Run DEV server

```bash
npm run dev
```

## Build dist

```bash
npm run build
```

## Deploy to gh-pages branch

```bash
npm run deploy
```

## License

Code released under the [MIT License](https://gitlab.com/eugenekyselev/erc20-generator/blob/master/LICENSE).
