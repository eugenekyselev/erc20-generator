module.exports = {
  title: 'ERC20 Token Generator | Build your Token',
  description: 'Easily deploy Smart Contract for a Standard, Capped, Mintable, Burnable, Payable ERC20 Token.',
  base: '/erc20-generator/',
  ga: 'UA-115756440-2',
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:url', content: 'https://eugenekyselev.gitlab.io/erc20-generator' }],
    ['meta', { property: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { property: 'twitter:title', content: 'ERC20 Token Generator | Build your Token' }],
    ['script', { src: 'assets/js/web3.min.js' }],
  ],
  defaultNetwork: 'mainnet',
};
